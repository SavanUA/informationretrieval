package edu.arizona.cs;

import java.util.List;
import java.util.ListIterator;
import java.util.Stack;

/**
 * Created by Savan Kiran on 01/25/17.
 */
public class QueryRunner {
    private Tokenizer tokenizer;
    private InvertedIndex invertedIndex;

    public QueryRunner(InvertedIndex invertedIndex) {
        this.invertedIndex = invertedIndex;
        tokenizer = new Tokenizer();
    }

    public List<InvertedIndex.ProximityAnswer> runQuery(String queryStr) {
        List<String> tokens = tokenizer.tokenize(queryStr);
        List<InvertedIndex.ProximityAnswer> answer = null;

        System.out.println("Query: " + queryStr);

        if(tokens.size() >= 3) {
            String word1 = tokens.get(0);
            String arg = tokens.get(1);
            String word2 = tokens.get(2);

            int direction = 0;
            int distance;
            if(tokens.size() > 3) {
                String directionStr = tokens.get(3);
                direction = Integer.parseInt(directionStr);
            }

            String distanceStr = arg.substring(1);
            distance = Integer.parseInt(distanceStr);

            answer = invertedIndex.proximitySearch(word1, word2, distance, direction!=0);
        }

        return answer;
    }
}
