package edu.arizona.cs;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Created by Savan Kiran on 01/25/17.
 */
public class InvertedIndex {
    private static final String DOC_NAME_PREFIX = "doc";

    private HashMap<String, List<PositionalIndex>> ii;
    private List<String> docs;

    private Tokenizer tokenizer;

    public InvertedIndex(String filePath) {
        tokenizer = new Tokenizer();
        ii = new HashMap<>();
        docs = new ArrayList<>();

        Path file = FileSystems.getDefault().getPath(filePath);

        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
            String line;
            while ((line = reader.readLine()) != null) {
                List<String> tokens = tokenizer.tokenize(line);
                String doc = tokens.get(0);
                docs.add(doc);

                for (int i = 1; i < tokens.size(); i++) {
                    if(!ii.containsKey(tokens.get(i))) {
                        // if dictionary doesn't contain the word yet
                        // create a postings list and add it with the word to inverted index
                        // postings list will contain positional information
                        List<PositionalIndex> postings = new LinkedList<>();
                        PositionalIndex positionalIndex = new PositionalIndex(extractId(doc));
                        positionalIndex.addPosition(i);
                        postings.add(positionalIndex);

                        ii.put(tokens.get(i), postings);
                    } else {
                        // if dictionary already contains the word
                        // get the postings list for the word and append this doc to the postings list
                        // postings list will contain positional information
                        List<PositionalIndex> postings = ii.get(tokens.get(i));
                        PositionalIndex positionalIndex = null;

                        for(PositionalIndex pi: postings) {
                            if(pi.getDocId() == extractId(doc)) {
                                positionalIndex = pi;
                                break;
                            }
                        }

                        if(positionalIndex == null) {
                            positionalIndex = new PositionalIndex(extractId(doc));
                            postings.add(positionalIndex);
                        }

                        positionalIndex.addPosition(i);
                    }
                }
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    private int extractId(String doc) {
        String idStr = doc.substring(3);
        return Integer.parseInt(idStr);
    }

    private String constructDocName(int docId) {
        return DOC_NAME_PREFIX + docId;
    }

    public List<ProximityAnswer> proximitySearch(String word1, String word2, int distance, boolean direction) {
        List<ProximityAnswer> answer = new LinkedList<>();

        List<PositionalIndex> postings1 = ii.get(word1);
        List<PositionalIndex> postings2 = ii.get(word2);

        for (int i=0, j=0; i < postings1.size() && j < postings2.size(); ) {
            int doc1 = postings1.get(i).getDocId();
            int doc2 = postings2.get(j).getDocId();

            if(doc1 == doc2) {
                List<Integer> positionsWord1 = findPositionalIndex(postings1, doc1).getPositions();
                List<Integer> positionsWord2 = findPositionalIndex(postings2, doc1).getPositions();
                List<Integer> list = new LinkedList<>();

                for (int p = 0; p < positionsWord1.size(); ) {
                    int pos1 = positionsWord1.get(p);

                    for (int q = 0; q < positionsWord2.size(); ) {
                        int pos2 = positionsWord2.get(q);

                        if(!direction) {
                            if (Math.abs(pos1 - pos2) <= distance) {
                                list.add(pos2);
                            } else if (pos2 > pos1) {
                                break;
                            }
                        } else {
                            if(pos2 > pos1 && pos2-pos1 <= distance) {
                                list.add(pos2);
                            } else if(pos1 > pos2) {
                                break;
                            }
                        }

                        q++;
                    }

                    if(!direction) {
                        while (list.size() > 0 && Math.abs(list.get(0) - pos1) > distance) {
                            list.remove(0);
                        }
                    } else {
                        while (list.size() > 0 && list.get(0) > pos1 && list.get(0) - pos1 > distance) {
                            list.remove(0);
                        }
                    }

                    for (int k = 0; k < list.size(); k++) {
                        answer.add(new ProximityAnswer(doc1, pos1, list.get(k)));
                    }

                    p++;
                }

                i++;
                j++;
            } else if(doc1 < doc2) {
                i++;
            } else {
                j++;
            }
        }

        return answer;
    }

    private PositionalIndex findPositionalIndex(List<PositionalIndex> postings, int docId) {
        PositionalIndex positionalIndex = null;

        for (int i = 0; i < postings.size(); i++) {
            if(postings.get(i).getDocId() == docId) {
                positionalIndex = postings.get(i);
                break;
            }
        }

        return positionalIndex;
    }

    /**
     * Maps document with list of positions of a particular word occurrences
     */
    private class PositionalIndex {
        private int docId;
        private List<Integer> positions;

        public PositionalIndex(int docId) {
            this.docId = docId;
            positions = new LinkedList<>();
        }

        public int getDocId() {
            return docId;
        }

        public List<Integer> getPositions() {
            return positions;
        }

        public void addPosition(int position) {
            positions.add(position);
        }
    }

    /**
     * Answer tuple with document, and positions of two words
     */
    public class ProximityAnswer {
        private int docId;
        private int positionWord1;
        private int positionWord2;

        public ProximityAnswer(int docId, int position1, int position2) {
            this.docId = docId;
            this.positionWord1 = position1;
            this.positionWord2 = position2;
        }

        public int getDocId() {
            return docId;
        }

        public String getDocName() {
            return constructDocName(docId);
        }

        public int getPositionWord1() {
            return positionWord1;
        }

        public int getPositionWord2() {
            return positionWord2;
        }
    }
}
