package edu.arizona.cs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Savan Kiran on 01/25/17.
 */
public class Main {

    private static final String QUIT = "q";

    private static InvertedIndex invertedIndex;
    private static QueryRunner queryRunner;

    public static void main(String[] args) {
        invertedIndex = new InvertedIndex(args[0]);
        queryRunner = new QueryRunner(invertedIndex);

        System.out.println("Enter query or 'Q' to quit ...");
        System.out.println("Ex: new /2 drug     ---- Non-directional");
        System.out.println("Ex: new /2 drug 0   ---- Non-directional");
        System.out.println("Ex: new /2 drug 1   ---- Directional");

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            try {
                String query = br.readLine();
                if(query.compareToIgnoreCase(QUIT) == 0) {
                    break;
                }

                try {
                    List<InvertedIndex.ProximityAnswer> answer = queryRunner.runQuery(query);
                    ListIterator<InvertedIndex.ProximityAnswer> answerIterator = answer.listIterator();

                    while (answerIterator.hasNext()) {
                        InvertedIndex.ProximityAnswer proximityAnswer = answerIterator.next();
                        System.out.print("\t" + proximityAnswer.getDocName() + " - " +
                                "(" + proximityAnswer.getPositionWord1() + "," + proximityAnswer.getPositionWord2() + ")" +
                                "\n");
                    }
                } catch (Exception e) {
                    System.out.println("Wrong query: " + query);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
