----Inverted Index----

1. Compilation
	a. Use the script.sh bash script to compile the source code.
	You might have to give executable permissions for the script.
	b. This generates a Ass2_Prob7.jar executable in the project root.
2. Execution
	a. Please use the following command to execute the program
		java -jar Ass2_Prob7.jar <path_to_docs.txt>
		
		ex: java -jar Ass2_Prob7.jar docs/docs.txt
	b. The program uses paranthesis to disambiguate precedence of operations.
		
        Enter query or 'Q' to quit ...
        Ex: new /2 drug     ---- Non-directional
        Ex: new /2 drug 0   ---- Non-directional
        Ex: new /2 drug 1   ---- Directional


