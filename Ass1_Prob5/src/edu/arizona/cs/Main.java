package edu.arizona.cs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Savan Kiran on 01/25/17.
 */
public class Main {

    private static final String QUIT = "q";

    private static InvertedIndex invertedIndex;
    private static QueryRunner queryRunner;

    public static void main(String[] args) {
        invertedIndex = new InvertedIndex(args[0]);
        queryRunner = new QueryRunner(invertedIndex);

        invertedIndex.print();

        /*List<String> answer = invertedIndex.x_OR_NOT_y("new", "pink");
        ListIterator<String> answerIterator = answer.listIterator();

        while (answerIterator.hasNext()) {
            System.out.print("\t" + answerIterator.next() + "\n");
        }*/

        System.out.println("Enter query or 'Q' to quit ...");
        System.out.println("Use paranthesis to disambiguate precedence of operations");
        System.out.println("Ex: (cat or dog) and food ----- Correct");
        System.out.println("Ex: cat or dog and food ------- Incorrect");

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            try {
                String query = br.readLine();
                if(query.compareToIgnoreCase(QUIT) == 0) {
                    break;
                }

                try {
                    List<String> answer = queryRunner.runQuery(query);
                    ListIterator<String> answerIterator = answer.listIterator();

                    while (answerIterator.hasNext()) {
                        System.out.print("\t" + answerIterator.next() + "\n");
                    }
                } catch (Exception e) {
                    System.out.println("Wrong query: " + query);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
