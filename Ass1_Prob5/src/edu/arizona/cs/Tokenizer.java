package edu.arizona.cs;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Savan Kiran on 01/25/17.
 */
public class Tokenizer {
    public List<String> tokenize(String documentStr) {
        List<String> tokens = new ArrayList<>();
        StringTokenizer stringTokenizer = new StringTokenizer(documentStr);

        while(stringTokenizer.hasMoreTokens()) {
            String potentialToken = stringTokenizer.nextToken().toLowerCase();

            while (potentialToken.startsWith("(")) {
                tokens.add("(");
                potentialToken = potentialToken.substring(1);
            }

            int firstIndexOfClosingBraces = potentialToken.indexOf(')');
            if(firstIndexOfClosingBraces != -1) {
                tokens.add(potentialToken.substring(0, firstIndexOfClosingBraces));
            } else {
                tokens.add(potentialToken);
            }

            while (potentialToken.endsWith(")")) {
                tokens.add(")");
                potentialToken = potentialToken.substring(0, potentialToken.length()-1);
            }
        }

        return tokens;
    }
}
