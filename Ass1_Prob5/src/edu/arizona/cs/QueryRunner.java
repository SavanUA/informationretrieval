package edu.arizona.cs;

import java.util.List;
import java.util.ListIterator;
import java.util.Stack;

/**
 * Created by Savan Kiran on 01/25/17.
 */
public class QueryRunner {
    private static final String AND = "and";
    private static final String OR = "or";
    private static final String OPEN_PARANTHESIS = "(";
    private static final String CLOSE_PARANTHESIS = ")";

    private Tokenizer tokenizer;
    private InvertedIndex invertedIndex;

    private Stack<QueryArguments> queryStack;
    private Stack<String> binaryOperandsStack;

    public QueryRunner(InvertedIndex invertedIndex) {
        this.invertedIndex = invertedIndex;
        tokenizer = new Tokenizer();
        queryStack = new Stack<>();
        binaryOperandsStack = new Stack<>();
    }

    public List<String> runQuery(String queryStr) {
        List<String> tokens = tokenizer.tokenize(queryStr);
        List<String> answer;

        System.out.println("Query: " + queryStr);
        ListIterator<String> tokensIterator = tokens.listIterator();

        boolean isAndOpAdded = false;
        boolean isOrOpAdded = false;

        while(tokensIterator.hasNext()) {
            String token = tokensIterator.next();

            switch (token) {
                case AND:
                    if(!isAndOpAdded) {
                        binaryOperandsStack.push(AND);
                        isAndOpAdded = true;
                    }
                    break;

                case OR:
                    if(!isOrOpAdded) {
                        binaryOperandsStack.push(OR);
                        isOrOpAdded = true;
                    }
                    break;

                case OPEN_PARANTHESIS:
                    queryStack.push(new QueryArguments(token));
                    break;

                case CLOSE_PARANTHESIS:
                    QueryArguments arg1 = queryStack.pop();
                    QueryArguments arg2 = queryStack.pop();
                    List<String> intermediate_answer;
                    do {
                        intermediate_answer = evaluate(arg1, arg2);
                        arg1 = new QueryArguments(intermediate_answer);
                        arg2 = queryStack.pop();
                    } while ((arg2.getWord() == null) ||
                            (arg2.getWord() != null && arg2.getWord().compareTo(OPEN_PARANTHESIS) != 0));

                    queryStack.push(arg1);
                    binaryOperandsStack.pop();

                    isAndOpAdded = false;
                    isOrOpAdded = false;
                    break;

                default:
                    queryStack.push(new QueryArguments(token));
                    break;
            }
        }

        if(queryStack.size() > 1 && !binaryOperandsStack.isEmpty()) {
            QueryArguments arg1 = queryStack.pop();
            QueryArguments arg2 = queryStack.pop();
            List<String> intermediate_answer;
            do {
                intermediate_answer = evaluate(arg1, arg2);
                arg1 = new QueryArguments(intermediate_answer);
                arg2 = queryStack.size()>0 ? queryStack.pop() : null;
            } while (arg2 != null);

            queryStack.push(arg1);
            binaryOperandsStack.pop();
        }

        answer = queryStack.pop().getPostings();

        return answer;
    }

    private List<String> evaluate(QueryArguments arg1, QueryArguments arg2) {
        List<String> answer;

        if (arg1.getWord() == null || arg2.getWord() == null) {
            if (arg1.getWord() == null && arg2.getWord() == null) {
                answer = binaryOperandsStack.peek().compareTo(AND) == 0 ?
                        invertedIndex.AND(arg1.getPostings(), arg2.getPostings()) :
                        invertedIndex.OR(arg1.getPostings(), arg2.getPostings());
            } else if (arg1.getWord() == null) {
                answer = binaryOperandsStack.peek().compareTo(AND) == 0 ?
                        invertedIndex.AND(arg2.getWord(), arg1.getPostings()) :
                        invertedIndex.OR(arg2.getWord(), arg1.getPostings());
            } else {
                answer = binaryOperandsStack.peek().compareTo(AND) == 0 ?
                        invertedIndex.AND(arg1.getWord(), arg2.getPostings()) :
                        invertedIndex.OR(arg1.getWord(), arg2.getPostings());
            }
        } else {
            answer = binaryOperandsStack.peek().compareTo(AND) == 0 ?
                    invertedIndex.AND(arg1.getWord(), arg2.getWord()) :
                    invertedIndex.OR(arg1.getWord(), arg2.getWord());
        }

        return answer;
    }

    static private class QueryArguments {
        private String word;
        private List<String> postings;

        public QueryArguments(String word) {
            this.word = word;
            this.postings = null;
        }

        public QueryArguments(List<String> postings) {
            this.postings = postings;
            this.word = null;
        }

        public String getWord() {
            return word;
        }

        public List<String> getPostings() {
            return postings;
        }
    }
}
