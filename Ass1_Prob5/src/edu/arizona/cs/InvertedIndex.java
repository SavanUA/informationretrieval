package edu.arizona.cs;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Created by Savan Kiran on 01/25/17.
 */
public class InvertedIndex {
    private static final String DOC_NAME_PREFIX = "doc";

    private HashMap<String, List<String>> ii;
    private List<String> docs;

    private Tokenizer tokenizer;

    public InvertedIndex(String filePath) {
        tokenizer = new Tokenizer();
        ii = new HashMap<>();
        docs = new ArrayList<>();

        Path file = FileSystems.getDefault().getPath(filePath);

        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
            String line;
            while ((line = reader.readLine()) != null) {
                List<String> tokens = tokenizer.tokenize(line);
                docs.add(tokens.get(0));

                for (int i = 1; i < tokens.size(); i++) {
                    if(!ii.containsKey(tokens.get(i))) {
                        // if dictionary doesn't contain the word yet
                        // create a postings list and add it with the word to inverted index
                        List<String> postings = new LinkedList<>();
                        postings.add(tokens.get(0));
                        ii.put(tokens.get(i), postings);
                    } else {
                        // if dictionary already contains the word
                        // get the postings list for the word and append this doc to the postings list
                        List<String> postings = ii.get(tokens.get(i));
                        if(!postings.contains(tokens.get(0)))
                            postings.add(tokens.get(0));
                    }
                }
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    public void print() {
        System.out.println("---- Inverted Index ----");
        for(Map.Entry<String, List<String>> entry : ii.entrySet()) {
            System.out.print(entry.getKey() + "->");
            List<String> postings = entry.getValue();
            ListIterator<String> postingsIterator = postings.listIterator();
            while(postingsIterator.hasNext()) {
                System.out.print(postingsIterator.next() + (postingsIterator.hasNext()?",":"\n"));
            }
        }
        System.out.println();
    }

    private int extractId(String doc) {
        String idStr = doc.substring(3);
        return Integer.parseInt(idStr);
    }

    private String constructDocName(int docId) {
        return DOC_NAME_PREFIX + docId;
    }

    public List<String> AND(List<String> postings1, List<String> postings2) {
        List<String> answer = new LinkedList<>();

        for (int i = 0, j=0; i < postings1.size() && j < postings2.size(); ) {
            int doc1 = extractId(postings1.get(i));
            int doc2 = extractId(postings2.get(j));

            if(doc1 == doc2) {
                answer.add(constructDocName(doc1));
                i++;
                j++;
            } else if(doc1 < doc2) {
                i++;
            } else {
                j++;
            }
        }

        return answer;
    }

    public List<String> AND(String word1, String word2) {
        List<String> answer = new LinkedList<>();
        List<String> postings1, postings2;

        postings1 = ii.get(word1);
        postings2 = ii.get(word2);

        answer = AND(postings1, postings2);

        return answer;
    }

    public List<String> AND(String word1, List<String> postings2) {
        List<String> answer;
        List<String> postings1;

        postings1 = ii.get(word1);

        answer = AND(postings1, postings2);

        return answer;
    }

    public List<String> OR(List<String> postings1, List<String> postings2) {
        List<String> answer = new LinkedList<>();

        int i, j;
        for (i = 0, j=0; i < postings1.size() && j < postings2.size(); ) {
            int doc1 = extractId(postings1.get(i));
            int doc2 = extractId(postings2.get(j));

            if(doc1 == doc2) {
                answer.add(constructDocName(doc1));
                i++;
                j++;
            } else if(doc1 < doc2) {
                answer.add(constructDocName(doc1));
                i++;
            } else {
                answer.add(constructDocName(doc2));
                j++;
            }
        }

        while(i < postings1.size()) {
            answer.add(postings1.get(i));
            i++;
        }

        while(j < postings2.size()) {
            answer.add(postings2.get(j));
            j++;
        }
        return answer;
    }

    public List<String> OR(String word1, String word2) {
        List<String> answer;
        List<String> postings1, postings2;

        postings1 = ii.get(word1);
        postings2 = ii.get(word2);

        answer = OR(postings1, postings2);

        return answer;
    }

    public List<String> OR(String word1, List<String> postings2) {
        List<String> answer;
        List<String> postings1;

        postings1 = ii.get(word1);

        answer = OR(postings1, postings2);

        return answer;
    }

    public List<String> x_AND_NOT_y(String x, String y) {
        List<String> answer;
        List<String> postingsX, postingsY;

        postingsX = ii.get(x);
        postingsY = ii.get(y);

        answer = x_AND_NOT_y(postingsX, postingsY);

        return answer;
    }

    public List<String> x_AND_NOT_y(List<String> postingsX, List<String> postingsY) {
        List<String> answer = new LinkedList<>();

        int i, j;
        for (i = 0, j=0; i < postingsX.size() && j < postingsY.size(); ) {
            int docX = extractId(postingsX.get(i));
            int docY = extractId(postingsY.get(j));

            if(docX == docY) {
                i++;
                j++;
            } else if(docX < docY) {
                answer.add(constructDocName(docX));
                i++;
            } else {
                j++;
            }
        }

        while(i < postingsX.size()) {
            answer.add(postingsX.get(i));
            i++;
        }

        return answer;
    }

    public List<String> x_OR_NOT_y(String x, String y) {
        List<String> answer;
        List<String> postingsX, postingsY;

        postingsX = ii.get(x);
        postingsY = ii.get(y);

        answer = x_OR_NOT_y(postingsX, postingsY);

        return answer;
    }

    public List<String> x_OR_NOT_y(List<String> postingsX, List<String> postingsY) {
        List<String> answer = new LinkedList<>();

        int i, j;
        for (i=0, j=0; i < postingsX.size() && j < postingsY.size(); ) {
            int docX = extractId(postingsX.get(i));
            int docY = extractId(postingsY.get(j));

            if(docX == docY) {
                answer.add(constructDocName(docX));

                int lower = j>0?extractId(postingsY.get(j-1)):0;
                int upper = docY;

                // Because the docs ids are assumed to contiguous
                for (int k = lower+1; k < upper; k++) {
                    // add all docs that aren't in Y
                    String doc = constructDocName(k);
                    if(!answer.contains(doc))
                        answer.add(doc);
                }

                i++;
                j++;
            } else if(docX < docY) {
                answer.add(constructDocName(docX));

                if(i==0 && j==0) {
                    //Add docs prior to docX which aren't present in Y
                    for (int k = 1; k < docX; k++) {
                        answer.add(constructDocName(k));
                    }
                }

                i++;
            } else {
                int lower = j>0?extractId(postingsY.get(j-1)):0;
                int upper = docY;

                // Because the docs ids are assumed to contiguous
                for (int k = lower+1; k < upper; k++) {
                    // add all docs that aren't in Y
                    String doc = constructDocName(k);
                    if(!answer.contains(doc))
                        answer.add(doc);
                }

                j++;
            }
        }

        while(i < postingsX.size()) {
            answer.add(postingsX.get(i));
            i++;
        }

        while(j < postingsY.size()+1) {
            int lower = j>0?extractId(postingsY.get(j-1)):0;
            int upper = j<postingsY.size()?extractId(postingsY.get(j)):(docs.size()+1);

            // Because the docs ids are assumed to contiguous
            for (int k = lower+1; k < upper; k++) {
                // add all docs that aren't in Y
                String doc = constructDocName(k);
                if(!answer.contains(doc))
                    answer.add(constructDocName(k));
            }

            j++;
        }

        return answer;
    }

    public List<String> x_AND_NOT_y_Naive(List<String> postingsX, List<String> postingsY) {
        List<String> answer = new LinkedList<>();
        List<String> not_postingsY = new LinkedList<>();

        for(int k=0;k<docs.size();k++) {
            String doc = docs.get(k);
            if(!postingsY.contains(doc)) {
                not_postingsY.add(doc);
            }
        }

        answer = AND(postingsX, not_postingsY);

        return answer;
    }

}
