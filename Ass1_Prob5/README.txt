----Inverted Index----

1. Compilation
	a. Use the script.sh bash script to compile the source code.
	You might have to give executable permissions for the script.
	b. This generates a Ass1_Prob5.jar executable in the project root.
2. Execution
	a. Please use the following command to execute the program
		java -jar Ass1_Prob5.jar <path_to_docs.txt>
		
		ex: java -jar Ass1_Prob5.jar docs/docs.txt
	b. The program uses paranthesis to disambiguate precedence of operations.
		
		Enter query or 'Q' to quit ...
		Use paranthesis to disambiguate precedence of operations
		Ex: (cat or dog) and food ----- Correct
		Ex: cat or dog and food ------- Incorrect

